#include <pylon_config.h>
#include <vars.h>

void initGSM() {
    pinMode(MODEM_PWKEY, OUTPUT);
    pinMode(MODEM_RST, OUTPUT);
    pinMode(MODEM_POWER_ON, OUTPUT);
    digitalWrite(MODEM_PWKEY, LOW);
    digitalWrite(MODEM_RST, HIGH);
    digitalWrite(MODEM_POWER_ON, HIGH);

    SerialAT.begin(9600, SERIAL_8N1, MODEM_RX, MODEM_TX);
    delay(3000);

    // Restart takes quite some time
    // To skip it, call init() instead of restart()
    Serial.println(F("Initializing GSM Modem..."));
    gsmModem.restart();
    // modem.init();

    String modemInfo = gsmModem.getModemInfo();
    Serial.print(F("GSM Modem Info: "));
    Serial.println(modemInfo);

    // Unlock your SIM card with a PIN if needed
    if (GSM_PIN && gsmModem.getSimStatus() != 3) {
        gsmModem.simUnlock(GSM_PIN);
    }
}

void initJson() {
  jsondoc.clear();
  jsondoc.garbageCollect();
  jsondoc["id"] = WiFi.macAddress();
  jsondoc["hardware_serial"] = config.thingsSerial;
}

void initBleGateway() {
  BLEDevice::init("");
  pBLEScan = BLEDevice::getScan();
  pBLEScan->setActiveScan(true); //active scan uses more power, but get results faster
  pBLEScan->setInterval(100);
  pBLEScan->setWindow(99);  // less or equal setInterval value
}

bool initFlashMemory() {
  if(!SPIFFS.begin(true)){
    Serial.println(F("An Error has occurred while mounting Flash Memory."));
    return false;
  }
  
  File file = SPIFFS.open("/config.txt", "r");
  if(!file){
    Serial.println(F("Failed to open config file."));
    return false;
  }
  
  String currentLine;
  int _cnt = 0;
  
  while(file.available()){
    char c = file.read();
    if (c == '\n') {
      //Serial.println(currentLine);
      if (_cnt == 0) {
        config.portalPassword = currentLine;
      } else if (_cnt == 1) {
        config.ssid = currentLine;
      } else if (_cnt == 2) {
        config.password = currentLine;
      } else if (_cnt == 3) {
        config.thingsUser = currentLine;
      } else if (_cnt == 4) {
        config.thingsPassword = currentLine;
      } else if (_cnt == 5) {
        config.thingsSerial = currentLine;
      } else if (_cnt == 6) {
        config.scanInterval = currentLine.toInt();
      } else if (_cnt == 7) {
        config.scanDuration = currentLine.toInt();
      }
      currentLine = "";
      _cnt++;
    } else if (c != '\r') {
      currentLine += c;
    }
  }
  file.close();
  
  return true;
}

bool initWifi() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(config.ssid.c_str(), config.password.c_str());

  int _timeout = 20; // * in seconds
  bool isConnected = true;
  Serial.println(F("Connecting to WiFi.."));
  
  while (WiFi.status() != WL_CONNECTED) {
    if (_timeout < 0) {
      isConnected = false;
      break;
    }
    _timeout--;
    delay(1000);
  }
  
  if (!isConnected) {
    Serial.print(F("WiFi Connection Failed = "));
    Serial.println(WiFi.status());
    return false;
  }
  
  Serial.println(F("Connected to the WiFi network."));
  Serial.print(F("IP address:\t"));
  Serial.println(WiFi.localIP()); 
  
  return true;
}

void initWifiAP() {
  Serial.println(F("Starting Web Server..."));
  
  WiFi.mode(WIFI_AP);
  WiFi.softAP(config.thingsSerial.c_str(), config.portalPassword.c_str());
  delay(3000); // Fix Crash
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));

  dnsServer.start(53, "*", apIP);
  server.begin();

  Serial.println(F("Started Web Server."));
}