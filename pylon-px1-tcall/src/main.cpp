#include <pylon_config.h>
#include <initialization.h>
#include <functions.h>
#include <vars.h>

configuration config = configuration();

// * Enabling Variables
bool _isEnabledAP = false;
bool _isEnabledScan = false;

// * Objects
WiFiServer server(80);
WiFiClient webclient;
WiFiClient espClient;
// #ifdef DUMP_AT_COMMANDS
//   #include <StreamDebugger.h>
//   StreamDebugger debugger(SerialAT, Serial);
//   TinyGsm gsmModem(debugger);
// #else
TinyGsm gsmModem(SerialAT);
// #endif
TinyGsmClient gsmClient(gsmModem, 2);
ThingsPH_MQTT thingsph(gsmClient); // * Defaults to gsmClient
StaticJsonDocument<256> jsondoc;

// * GSM
bool _isGPRSConnection = true;

// * WiFi Captive Portal
IPAddress apIP(192, 168, 1, 1);
DNSServer dnsServer;

// * Scan
VirtualDelay tmrScanner;

// * WiFi
bool _wifiConnected = false;
bool _isWifiScanning = false;
String _wifiScanned = "";
int _wifiScannedCount = 0;

// * ThingsPH MQTT
String _clientName;

// * BLE Scanner
BLEScan* pBLEScan;
bool _isBleScanning = false;
bool _isTimerRunning = false;
VirtualDelay tmrIntScanner;
int _bleCount = 0;





void setup() {
  Serial.begin(115200);

  // * Setup pins used by the system:
  pinMode(PIN_CONFIG, INPUT);
  pinMode(PIN_LED, OUTPUT);
  digitalWrite(PIN_LED, LOW);

  // * Initialize GSM Module
  initGSM();

  // * Initialize ThingsPH MQTT Connection
  thingsph.initialize(512);

  // * Load Configurations from Flash Memory:
  if (!initFlashMemory()) {
    Serial.println(F("Unable to continue! SPIFFS configuration failed."));   
    // * If failed to load, make sure to prevent system from continuing further:
    while (true) {
      // ? Blinking built in led means that the system failed to load
      digitalWrite(PIN_LED, HIGH);
      delay(1000);
      digitalWrite(PIN_LED, LOW);
      delay(1000);
    }
  }

  // * Initialize Json Document:
  initJson();

  // * Check if the user is trying to access the Captive Portal:
  bool _isAP = false;
  if (digitalRead(PIN_CONFIG) == HIGH) {
    _isAP = true;
  }
  
  delay(1000); // ? Create a 1 sec delay to make sure that the user didn't accidentally pushed the button

  // * If the wifi reset button is still in pushed state, continue to WiFi Captive Portal
  if (digitalRead(PIN_CONFIG) == HIGH && _isAP) {
    // * Setup the WiFi Captive Portal
    digitalWrite(PIN_LED, HIGH);
    _isEnabledAP = true;
    initWifiAP();
  }
  
  // * Initialize Ble Scanner
  initBleGateway();

  // * Set MQTT Client Name
  _clientName = CLIENT_NAME_PREFIX + WiFi.macAddress();
  _clientName.replace(":","");
}

void loop() {
  // * WiFi Captive Portal:
  if (_isEnabledAP) {
    dnsServer.processNextRequest();
    webclient = server.available(); 

    if (webclient) {
      String currentLine = "";
      String header = "";
      String configline = "";
      while (webclient.connected()) {
        if (webclient.available()) {
          char c = webclient.read();
          header += c;
          if (c == '\n') {
            if (currentLine.indexOf("GET /submit?") >= 0) {
              configline = currentLine;
            }
            if (currentLine.length() == 0) {
              if (header.indexOf("GET /config") >= 0) {
                webclient.println("HTTP/1.1 200 OK");
                webclient.println("Content-type:text/html");
                webclient.println();
                webclient.print(getWebPortal());
                
              } else if (header.indexOf("GET /submit?") >= 0) {
                webclient.println("HTTP/1.1 200 OK");
                webclient.println("Content-type:text/html");
                webclient.println();
                if (parseConfig(configline)) {
                  webclient.println(shtml1 + shtml2 + shtml3);
                } else {
                  webclient.println(shtml1 + shtml2_5 + shtml3);
                }
              }
              
              webclient.println();
              break;
            } else {
              currentLine = "";
            }
          } else if (c != '\r') {
            currentLine += c;
          }
        }
      }
      header = "";
      webclient.stop();
    }

    return; // ? During Captive Portal, DO NOT continue below
  }

  if (_isGPRSConnection && !gsmModem.isGprsConnected()) {
    if (!connectGPRS()) {
      _isGPRSConnection = false;
    } else {
      thingsph.setClient(gsmClient);
    }
    vTaskDelay(100);
  }

  // * Establish WiFi Connection if GPRS Failed:
  if (!_isGPRSConnection && WiFi.status() != WL_CONNECTED) {
    Serial.println(F("Trying WiFi Connection..."));
    if (thingsph.isconnected()) thingsph.disconnect();
    _wifiConnected = false;

    if (!initWifi()) {
      Serial.println(F("WiFi connection failed. Reverting to GPRS Connection..."));
      _isGPRSConnection = true;
    } else {
      _wifiConnected = true;
      thingsph.setClient(espClient);
    }
    vTaskDelay(100);
  }

  // * Connect to ThingsPH Platform if not connected:
  if (!thingsph.isconnected()) {
    thingsph.connect(_clientName.c_str(), config.thingsUser.c_str(), config.thingsPassword.c_str(), config.thingsSerial.c_str());
    vTaskDelay(100);
  }
                    
  // * Start interval timer with {_scanInterval}:
  if (thingsph.isconnected() && !_isTimerRunning && !_isEnabledScan) {
    delay(1000);
    _isTimerRunning = true;
    Serial.print(F("Scanning Interval: "));
    Serial.println(config.scanInterval);
    tmrIntScanner.start(config.scanInterval);
  }

  // * When interval timer elapsed, start the ble scan:
  if (tmrIntScanner.elapsed() && !_isEnabledScan) {   
    _isTimerRunning = false;
    _isEnabledScan = true;

    // * Reset WiFi Vars:
    _wifiScanned.clear();
    _wifiScannedCount = 0;
    _bleCount = 0;

    tmrScanner.start(config.scanDuration);
    Serial.print(F("Scan Duration: "));
    Serial.println(config.scanDuration);
    Serial.println(F("Scanning for WiFi..."));
  }

  if (_isEnabledScan) {
    if (!_isWifiScanning) {
      _isWifiScanning = true;
      WiFi.scanNetworks(true, true); // * Start WiFi Scanning
    }

    if (!_isBleScanning) {
      _isBleScanning = true;
      // * Start BLE Scanning
      pBLEScan->start(config.scanDuration/1000);
      Serial.println(F("Scanning for BLE Devices..."));
    }
  }

  if (WiFi.scanComplete() > -1) {
    if (_isEnabledScan) {
      int n = WiFi.scanComplete();

      for (int i = 0; i <= n; i++) {
        if (_wifiScanned.indexOf(WiFi.BSSIDstr(i)) < 0) {
          _wifiScanned.concat(WiFi.BSSIDstr(i));
          _wifiScanned.concat(",");
          _wifiScannedCount++;
        }
      }
    }

    WiFi.scanDelete();
    _isWifiScanning = false;
  }

  // * This scanner will trigger after {scanDuration} milliseconds:
  if (tmrScanner.elapsed()) {
    _isEnabledScan = false;
    
    pBLEScan->stop();
    Serial.println("Stopped BLE Scan.");

    _isBleScanning = false;
    _bleCount = pBLEScan->getResults().getCount();

    jsondoc["ble"] = _bleCount;
    jsondoc["wifi"] = _wifiScannedCount;

    // * Make sure to clear these:
    pBLEScan->clearResults();
    WiFi.scanDelete();
  }

  if (thingsph.isconnected() && jsondoc.size() > 2) {
    if (thingsph.publish(jsondoc)) {
      Serial.println(F("Successfully Sent!"));
    } else {
      Serial.println(F("Unable to send payload!"));
    }
    initJson();
    espClient.flush();
  }

  // * Do not forget to loop ThingsPH_MQTT
  thingsph.loop();
  
  vTaskDelay(100); // ? reset esp watchdog
}