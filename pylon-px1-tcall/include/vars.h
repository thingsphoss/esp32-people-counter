#ifndef vars_h
#define vars_h

#include <headers.h>

typedef struct {
    String portalPassword;
    String ssid;
    String password;
    String thingsUser;
    String thingsPassword;
    String thingsSerial;
    int scanInterval;
    int scanDuration;
} configuration;

extern configuration config;

// * Enabling Variables
extern bool _isEnabledAP;

// * Objects
extern WiFiServer server;
extern WiFiClient webclient;
extern WiFiClient espClient;
extern ThingsPH_MQTT thingsph;
extern StaticJsonDocument<256> jsondoc;
extern TinyGsm gsmModem;
extern TinyGsmClient gsmClient;

// * WiFi Captive Portal
extern IPAddress apIP;
extern DNSServer dnsServer;

// * Scan
extern VirtualDelay tmrScanner;

// * WiFi
extern bool _wifiConnected;
extern bool _isWifiScanning;
extern String _wifiScanned;
extern int _wifiScannedCount;

// * ThingsPH MQTT
extern String _clientName;

// * BLE Scanner
extern BLEScan* pBLEScan;
extern bool _isBleScanning;
extern bool _isTimerRunning;
extern VirtualDelay tmrIntScanner;

// * Web Portal HTML Source Code:
const String html1 = ""
  "<!DOCTYPE html><html><head><title>Pylon X1 - Configuration</title></head><style>"
  "html, body {align-items: center;background: #f2f4f8;border: 0;display: flex;font-family: Helvetica, Arial, sans-serif;font-size: 16px;height: 100%;justify-content: center;margin: 0;padding: 0;}"
  "form {--background: white;--border: rgba(0, 0, 0, 0.125);--borderDark: rgba(0, 0, 0, 0.25);--borderDarker: rgba(0, 0, 0, 0.5);--bgColorH: 0;--bgColorS: 0%;--bgColorL: 98%;--fgColorH: 210;--fgColorS: 79%;--fgColorL: 46%;--shadeDark: 0.3;--shadeLight: 0.7;--shadeNormal: 0.5;--borderRadius: 0.125rem;--highlight: #306090;background: white;border: 1px solid var(--border);border-radius: var(--borderRadius);box-shadow: 0 1rem 1rem -0.75rem var(--border);display: flex;flex-direction: column;padding: 1rem;position: relative;overflow: hidden;width: 600px;}"
  "h2 {text-align: center;color: hsl(var(--fgColorH), var(--fgColorS), var(--fgColorL));}"
  "h3 {color: var(--borderDarker);}"
  ".column {float: left;width: 50%;height: 300px;}"
  ".row:after {content: "";display: table;clear: both;}"
  "label > span {color: var(--borderDarker);display: block;font-size: 0.825rem;margin-top: 0.625rem;order: 1;transition: all 0.25s;}"
  "input {border: 1px solid var(--border);border-radius: var(--borderRadius);box-sizing: border-box;font-size: .80rem;height: 2.25rem;line-height: 1.25rem;margin-top: 0.25rem;margin-right: 0.25rem;order: 2;padding: 0.25rem 0.5rem;width: 95%;transition: all 0.25s;}"
  ".input-right {width: 100%;}"
  "input[type=\"submit\"] {color: hsl(var(--bgColorH), var(--bgColorS), var(--bgColorL));background: hsl(var(--fgColorH), var(--fgColorS), var(--fgColorL));font-size: 0.75rem;font-weight: bold;margin-top: 0.625rem;order: 4;outline: 1px dashed transparent;outline-offset: 2px;padding-left: 0;width: 100%;text-transform: uppercase;}"
  "</style><body><form method=\"get\" action=\"/submit\"><h2>Pylon X1 - Configuration</h2><div class=\"row\"><div class=\"column\"><h3>WiFi & Bluetooth</h3>"
  "<label><span>Portal Password</span><input name=\"portalpass\" type=\"text\" value=\"";
const String html2 = "\"></label><label><span>SSID</span><input name=\"ssid\" type=\"text\" value=\"";
const String html3 = "\"></label><label><span>Password</span><input name=\"wifipass\" type=\"text\" value=\"";
const String html4 = "\"></label></div><div class=\"column\"><h3>Things PH Platform</h3><label><span>Username</span><input class=\"input-right\" name=\"thingsuser\" type=\"text\" value=\"";
const String html5 = "\"></label><label><span>Password</span><input class=\"input-right\" name=\"thingspass\" type=\"text\" value=\"";
const String html6 = "\"></label><label><span>Hardware Serial</span><input class=\"input-right\" name=\"thingsserial\" type=\"text\" value=\"";
const String html7 = "\"></label></div><div class=\"row\"><div class=\"column\" style=\"height: 210px;\"><h3>Gateway Settings</h3><label><span>Scan Interval (ms)</span><input name=\"gwint\" type=\"text\" value=\"";
const String html8 = "\"></label><label><span>Scan Duration (ms)</span><input name=\"gwdur\" type=\"text\" value=\"";
const String html9 = "\"></label></div></div><input type=\"submit\" value=\"Save Changes\"/></form><body></html>";

const String shtml1 = ""
  "<!DOCTYPE html><html><head><title>Pylon X1 - Configuration</title></head><style>html, body {align-items: center;background: #f2f4f8;border: 0;display: flex;font-family: Helvetica, Arial, sans-serif;font-size: 16px;height: 100%;justify-content: center;margin: 0;padding: 0;}.container {--background: white;--border: rgba(0, 0, 0, 0.125);--borderDark: rgba(0, 0, 0, 0.25);--borderDarker: rgba(0, 0, 0, 0.5);--bgColorH: 0;--bgColorS: 0%;--bgColorL: 98%;--fgColorH: 210;--fgColorS: 79%;--fgColorL: 46%;--shadeDark: 0.3;--shadeLight: 0.7;--shadeNormal: 0.5;--borderRadius: 0.125rem;--highlight: #306090;background: white;border: 1px solid var(--border);border-radius: var(--borderRadius);box-shadow: 0 1rem 1rem -0.75rem var(--border);display: flex;flex-direction: column;padding: 1rem;position: relative;overflow: hidden;width: 600px;}"
  "h2 {text-align: center;color: hsl(var(--fgColorH), var(--fgColorS), var(--fgColorL));}"
  "h4 {color: var(--borderDarker);}"
  "input {border: 1px solid var(--border);border-radius: var(--borderRadius);box-sizing: border-box;font-size: .80rem;height: 2.25rem;line-height: 1.25rem;margin-top: 0.25rem;margin-right: 0.25rem;order: 2;padding: 0.25rem 0.5rem;width: 95%;transition: all 0.25s;}"
  "input[type=\"button\"] {color: hsl(var(--bgColorH), var(--bgColorS), var(--bgColorL));background: hsl(var(--fgColorH), var(--fgColorS), var(--fgColorL));font-size: 0.75rem;font-weight: bold;margin-top: 0.625rem;order: 4;outline: 1px dashed transparent;outline-offset: 2px;padding-left: 0;width: 100%;text-transform: uppercase;}"
  "</style><body><div class=\"container\"><h2>Pylon X1 - Configuration</h2>";
const String shtml2 = "<h4 style=\"text-align:center;\">Successfully saved changes. Restart the device<br>for the changes to take effect.</h4>";
const String shtml2_5 = "<h4 style=\"text-align:center;\">An error has occured while saving changes.<br>Please try again.</h4>";
const String shtml3 = "<a href=\"/config\"><input type=\"button\" value=\"Go Back to Portal\"/></a></div><body></html>";

#endif