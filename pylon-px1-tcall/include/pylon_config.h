#ifndef pylon_config_h
#define pylon_config_h

#define CLIENT_NAME_PREFIX "pylonb1-gw-"
#define PIN_CONFIG 15
#define PIN_LED 2
#define DUMP_AT_COMMANDS // * Comment this to remove AT Commands Log at Serial Monitor

// TTGO T-Call pins
#define MODEM_RST 5
#define MODEM_PWKEY 4
#define MODEM_POWER_ON 23
#define MODEM_TX 27
#define MODEM_RX 26

// * Set GSM PIN, if any (leave empty if not needed)
#define GSM_PIN ""

// * Your GPRS credentials:
const char apn[] = "YourAPN";
const char gprsUser[] = "";
const char gprsPass[] = "";

#endif