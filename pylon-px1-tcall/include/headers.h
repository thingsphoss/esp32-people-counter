#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>
#include <WiFi.h>
#include <VirtualDelay.h>
#include <ThingsPH_MQTT.h>
#include <DNSServer.h>
#include <SPIFFS.h>

#define TINY_GSM_MODEM_SIM800
#define TINY_GSM_DEBUG Serial
#define SerialAT Serial1

#include <TinyGsmClient.h>