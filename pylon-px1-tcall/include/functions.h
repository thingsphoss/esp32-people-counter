#include <Arduino.h>
#include <ArduinoJson.h>

bool connectGPRS();
String urlDecode(String str);
unsigned char h2Int(char c);
bool parseConfig(String msg);
String getWebPortal();