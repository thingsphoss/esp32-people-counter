#include <vars.h>

unsigned char h2Int(char c) {
  if (c >= '0' && c <='9') {
    return((unsigned char)c - '0');
  }
  if (c >= 'a' && c <='f') {
    return((unsigned char)c - 'a' + 10);
  }
  if (c >= 'A' && c <='F') {
    return((unsigned char)c - 'A' + 10);
  }
  return(0);
}

String urlDecode(String str) {
  String encodedString = "";
  char c, code0, code1;
  
  for (int i =0; i < str.length(); i++) {
    c = str.charAt(i);
    if (c == '+') {
      encodedString+=' ';  
    } else if (c == '%') {
      i++;
      code0 = str.charAt(i);
      i++;
      code1 = str.charAt(i);
      c = (h2Int(code0) << 4) | h2Int(code1);
      encodedString += c;
      
    } else {
      encodedString += c;  
    }
    
    yield();
  }
 return encodedString;
}

bool parseConfig(String msg) {
  yield();  // ? reset esp watchdog
  
  // * Decode the url parameters:
  String input = urlDecode(msg);

  // * Parse the data from the parameters:
  config.portalPassword = input.substring(input.indexOf("=") + 1, input.indexOf("&"));
  input = input.substring(input.indexOf("&") + 1);
  
  config.ssid = input.substring(input.indexOf("=") + 1, input.indexOf("&"));
  input = input.substring(input.indexOf("&") + 1);
  
  config.password = input.substring(input.indexOf("=") + 1, input.indexOf("&"));
  input = input.substring(input.indexOf("&") + 1);
  
  config.thingsUser = input.substring(input.indexOf("=") + 1, input.indexOf("&"));
  input = input.substring(input.indexOf("&") + 1);
  
  config.thingsPassword = input.substring(input.indexOf("=") + 1, input.indexOf("&"));
  input = input.substring(input.indexOf("&") + 1);
  
  config.thingsSerial = input.substring(input.indexOf("=") + 1, input.indexOf("&"));
  input = input.substring(input.indexOf("&") + 1);
  
  config.scanInterval = input.substring(input.indexOf("=") + 1, input.indexOf("&")).toInt();
  input = input.substring(input.indexOf("&") + 1);
  
  config.scanDuration = input.substring(input.indexOf("=") + 1, input.indexOf(" ")).toInt();

  // * Save new data to the flash memory:
  File file = SPIFFS.open("/config.txt", "w");
  if(!file){
    Serial.println(F("Failed to open config file"));
    return false;
  } else {
    file.println(config.portalPassword);
    file.println(config.ssid);
    file.println(config.password);
    file.println(config.thingsUser);
    file.println(config.thingsPassword);
    file.println(config.thingsSerial);
    file.println(config.scanInterval);
    file.println(config.scanDuration);
    file.close();
    return true;
  }
}

String getWebPortal() {
  return html1 + String(config.portalPassword) + html2 + String(config.ssid) + html3 + String(config.password) + 
          html4 + String(config.thingsUser) + html5 + String(config.thingsPassword) + html6 + String(config.thingsSerial) + 
          html7 + String (config.scanInterval) + html8 + String(config.scanDuration) + html9;
}